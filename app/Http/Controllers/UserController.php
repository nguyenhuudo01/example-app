<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\URL;
use PhpOffice\PhpSpreadsheet\IOFactory;

class UserController extends Controller
{
    public function index()
    {
        $data = User::all();
        return $data;
    }

    public function phaTrang()
    {
        try {
            //phan trang 5 items
            $phTrang = User::paginate(5);

            return response()->json([
                    'Phan_Trang' => $phTrang
                ]
            );
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    // đọc file excel
    public function readExcel(Request $request)
    {
        $file = $request->file('excel_file');
        $path = $file->getRealPath();
        $spreadsheet = IOFactory::load($path); // Tải tệp từ đường dẫn Excel và trả về bảng tính
        $worksheet = $spreadsheet->getActiveSheet(); // Lấy sheet hiện tại
        $data = [];
        $headerRow = $worksheet->getRowIterator()->current(); // Lấy hàng đầu tiên (header)
        $headerCells = $headerRow->getCellIterator(); // Lặp qua các ô trong hàng đầu tiên
        $header = [];

        foreach ($headerCells as $cell) {
            $header[] = $cell->getValue(); // Lưu tên trường vào mảng header
        }

        foreach ($worksheet->getRowIterator() as $row) { // Sử dụng vòng lặp, lặp qua từng hàng (trừ hàng đầu tiên)
            if ($row->getRowIndex() !== 1) { // Bỏ qua hàng đầu tiên (header)
                $rowData = []; // Lưu dữ liệu của hàng đó
                $cellIterator = $row->getCellIterator(); // Lặp qua từng ô và lấy giá trị từng ô đó
                $cellIterator->setIterateOnlyExistingCells(false); // cả các ô trống

                foreach ($header as $field) { // Lặp qua các trường
                    $rowData[$field] = $cellIterator->current()->getValue(); // Gán giá trị của ô vào từng trường của rowData
                    $cellIterator->next();
                }
                /* điều kiện sau
                quantity >50, soluong chia hết cho 5
                giới tính 1 nam or 2 nữ
                kí tự email là hotmail
                */
                // Điều kiện lọc 1: Lấy các dòng có 'quantity' > 50
                if ($rowData['quantity'] > 50) {
                    // Điều kiện lọc 2: Lấy các dòng có 'soluong' chia hết cho 5
                    if ($rowData['soluong'] % 5 === 0) {
                        // Điều kiện lọc 3: Hiển thị giới tính 'nam' nếu sex=1 và 'nữ' nếu sex=2
                        if ($rowData['sex'] == 1) {
                            $rowData['sex'] = 'nam';
                        } elseif ($rowData['sex'] == 2) {
                            $rowData['sex'] = 'nữ';
                        }

                        // Điều kiện lọc 4: Lấy các dòng có phần email chứa chuỗi 'hotmail'
                        if (strpos(strtolower($rowData['email']), 'hotmail') !== false) {
                            $data[] = $rowData;
                        }
                    }
                }
            }

            // số item mỗi trang (page)
            $perPage =5;

            // trang hiện tại
            $currentPage =$request->input('page', 1);

            // phân trang và lấy dữ liệu
            $paginator = $this->paginateData($data, $perPage, $currentPage);

        }
        return response()->json([
            'data' => $paginator,
        ]);
    }

    public function paginateData(array $data, int $perPage, int $currentPage)
    {
        $total = count($data); // đếm tổng phần  tử trong mảng data

        // băt đầu lấy trang hiện tại: startIndex = (2 - 1) * 5 = 5
        $startIndex = ($currentPage -1) * $perPage;

        // trích xuất mảng con  chứa dữ liệu để hiện thị trang hiện tại
        // mảng con hiện thị trên trang hiện tại
        $sliceData = array_slice($data, $startIndex, $perPage);

        // tạo số trang và trả dữ liệu trong phạm vi phân trang
        //Các thông số khác trong mảng tham số cho phép xác định đường dẫn của trang hiện tại.
        $paginator =  new LengthAwarePaginator($sliceData, $total, $perPage, $currentPage, [
            'path' =>URL::current(),// trang đang sự dụng
        ]);
        return $paginator;
    }
}
