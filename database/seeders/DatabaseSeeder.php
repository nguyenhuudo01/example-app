<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $faker = Faker::create();
        foreach (range(1, 1000) as $index) {
            $now = now()->toDateTimeString();
            $sex = $faker->randomElement([1, 2]);
            $quantity = $faker->randomNumber(2);
            $soluong = $faker->randomNumber(2);
            DB::table('users')->insert([
                'name' => $faker->name,
                'sex' => $sex,
                'quantity' => $quantity,
                'soluong' => $soluong,
                'email' => $faker->email,
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }
    }
}
